# README #

CMPT 275 : TEAM 15

Team Members

 Ahmed Medhioub (Project Manager)

 Nathan Nguyen

 Michael Zhang

 Joey Parker

 Derek Cheng

This is the repository for Team15's (SFU Dev Squad) CMPT275 project.

How to clone:
git clone https://bitbucket.org/sfudevsquad/cmpt275-team15.git

How to update:
git update


